/**
 * 
 */
package ro.setmobile.sh2o.esb.processor;

import org.apache.log4j.Logger;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.message.MessageContentsList;

/**
 * @author dan
 *
 */
public class Sh2oProcessor implements Processor {
	static Logger log = Logger.getLogger(Sh2oProcessor.class.getName());
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
	 */
	@Override
	public void process(Exchange exchange) throws Exception {
		log.info("Hello this is an info message");
		log.info(exchange.getIn().getBody(String.class));
		String camelBody = "PLM";
		// The new body message will be a simple String which holds the
		// concatenated values of the MessageContentsList
		
		MessageContentsList cxfMessage = exchange.getIn().getBody(MessageContentsList.class);
		if (cxfMessage == null) {
			System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% BTP");
		}
		camelBody = exchange.getIn().getBody().toString();
		System.out.println("*************************************************** " + camelBody);
		exchange.getIn().setBody(camelBody);
	}

}

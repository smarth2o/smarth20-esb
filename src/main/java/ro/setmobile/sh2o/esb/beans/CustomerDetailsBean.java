package ro.setmobile.sh2o.esb.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "customerDetails")
public class CustomerDetailsBean {

	private String username;
	private String email;
	private String smartMeterID;
	private String familyID;
	private String zipcode;
	private String country;
	private String password;
	
	public CustomerDetailsBean() {
	}

	
	
	public CustomerDetailsBean(String username, String email,
			String smartMeterID, String familyID, String zipcode,
			String country, String password) {
		this.username = username;
		this.email = email;
		this.smartMeterID = smartMeterID;
		this.familyID = familyID;
		this.zipcode = zipcode;
		this.country = country;
		this.password = password;
	}



	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSmartMeterID() {
		return smartMeterID;
	}

	public void setSmartMeterID(String smartMeterID) {
		this.smartMeterID = smartMeterID;
	}

	public String getFamilyID() {
		return familyID;
	}

	public void setFamilyID(String familyID) {
		this.familyID = familyID;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

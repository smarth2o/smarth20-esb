/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/AssignActionsData")
public class AssignActionsData {

	@POST
	@Path("/assignActions")
	@Produces(MediaType.APPLICATION_JSON)
	public String assignActions(@FormParam("assignActions") String assignActions) {
		return null;
	}
}

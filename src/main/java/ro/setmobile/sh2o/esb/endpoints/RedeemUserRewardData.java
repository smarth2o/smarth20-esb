/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/RedeemUserRewardData")
public class RedeemUserRewardData {

	@POST
	@Path("/redeemUserReward")
	@Produces(MediaType.APPLICATION_JSON)
	public String redeemUserReward(@FormParam(" idReward") String idReward, @FormParam("userEmail") String userEmail) {
		return null;
	}
}

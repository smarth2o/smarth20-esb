/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/ConsumptionSummaryData")
public class ConsumptionSummaryData {

	@GET
	@Path("/getConsumptionSummary")
	@Produces(MediaType.APPLICATION_JSON)
	public String getConsumption(@QueryParam("user_id") String user_id) {
		return null;
	}
}

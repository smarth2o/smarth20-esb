/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/ConsumptionData")
public class ConsumptionData {

	@GET
	@Path("/getConsumption")
	@Produces(MediaType.APPLICATION_JSON)
	public String getConsumption(@QueryParam("user_id") String user_id) {
		return null;
	}
}

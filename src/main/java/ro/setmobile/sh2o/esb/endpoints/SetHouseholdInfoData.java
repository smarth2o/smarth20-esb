/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ro.setmobile.sh2o.esb.beans.CustomerDetailsBean;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/SetHouseholdInfoData")
public class SetHouseholdInfoData {

	@POST
	@Path("/setHouseholdInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String setHouseholdInfo(String json) {
		return null; 
	}
}

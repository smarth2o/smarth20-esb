/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/ValidSmartMeterIDData")
public class ValidSmartMeterIDData {

	@GET
	@Path("/checkExistingUsername")
	@Produces(MediaType.APPLICATION_JSON)
	public String checkValidSmartMeterID(@QueryParam("smartmeterid") String smartmeterid) {
		return null;
	}
}

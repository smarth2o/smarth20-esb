package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/GetThisMonthAverageConsumption")
public interface GetThisMonthAverageConsumption {

	@GET
	@Path("/getThisMonthAverageConsumption")
	@Produces(MediaType.APPLICATION_JSON)
	public String getThisMonthAverageConsumption(@QueryParam("user_id") String user_id);
	
	public String getAlerts(@QueryParam("user_id") String user_id);
}
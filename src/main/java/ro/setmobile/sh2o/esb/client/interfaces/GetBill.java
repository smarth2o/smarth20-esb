package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/GetBill")
public interface GetBill {

	@GET
	@Path("/getBill")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBill(@QueryParam("user_id") String user_id);

}
package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/GetGranularity")
public interface GetGranularity {

	@GET
	@Path("/getGranularity")
	@Produces(MediaType.APPLICATION_JSON)
	public String getGranularityData(@QueryParam("user_id") String user_id);

}
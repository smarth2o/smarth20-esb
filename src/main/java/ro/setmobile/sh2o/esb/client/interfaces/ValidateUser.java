package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/UserValidation")
public interface ValidateUser {

	@GET
	@Path("/validateUser")
	@Produces(MediaType.APPLICATION_JSON)
	public String validateUser(@QueryParam("user_email") String user_email, @QueryParam("user_password") String user_password);

}